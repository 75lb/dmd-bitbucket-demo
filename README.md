## handbrake-js
Handbrake for node.js.

**Example**  
```js
var hbjs = require("handbrake-js")
```

* [handbrake-js](#markdown-header-handbrakejs)
    * _static_
        * [.spawn([options])](#markdown-header-hbjsspawnoptions-modulehandbrakejshandbrake) ⇒ Handbrake
        * [.exec(options, [onComplete])](#markdown-header-hbjsexecoptions-oncomplete)
    * _inner_
        * [~Handbrake](#markdown-header-hbjshandbrake-externaleventemitter) ⇐ EventEmitter
            * [.output](#markdown-header-handbrakeoutput-string) : string
            * [.options](#markdown-header-handbrakeoptions-object) : object
            * [.eError](#markdown-header-handbrakeeerror)
            * ["start"](#markdown-header-start)
            * ["begin"](#markdown-header-begin)
            * ["progress" (progress)](#markdown-header-progressprogress)
            * ["output" (output)](#markdown-header-outputoutput)
            * ["error" (error)](#markdown-header-errorerror)
            * ["end"](#markdown-header-end)
            * ["complete"](#markdown-header-complete)

### hbjs.spawn([options]) ⇒ [Handbrake](#markdown-header-hbjshandbrake-externaleventemitter)
Spawns a HandbrakeCLI process with the supplied [options](https://trac.handbrake.fr/wiki/CLIGuide#options), returning an instance of `Handbrake` on which you can listen for events.

**Kind**: static method of [handbrake-js](#markdown-header-handbrakejs)  

| Param | Type | Description |
| --- | --- | --- |
| [options] | object | [Options](https://trac.handbrake.fr/wiki/CLIGuide#options) to pass directly to HandbrakeCLI |

**Example**  
```js
var hbjs = require("handbrake-js")

hbjs.spawn(options)
    .on("error", console.error)
    .on("output", console.log)
```
### hbjs.exec(options, [onComplete])
Runs HandbrakeCLI with the supplied [options](https://trac.handbrake.fr/wiki/CLIGuide#options) calling the supplied callback on completion. The exec method is best suited for short duration tasks where you can wait until completion for the output.

**Kind**: static method of [handbrake-js](#markdown-header-handbrakejs)  

| Param | Type | Description |
| --- | --- | --- |
| options | Object | [Options](https://trac.handbrake.fr/wiki/CLIGuide#options) to pass directly to HandbrakeCLI |
| [onComplete] | function | If passed, `onComplete(err, stdout, stderr)` will be called on completion, `stdout` and `stderr` being strings containing the HandbrakeCLI output. |

**Example**  
```js
var hbjs = require("handbrake-js")

hbjs.exec({ preset-list: true }, function(err, stdout, stderr){
    if (err) throw err
    console.log(stdout)
})
```
### hbjs~Handbrake ⇐ EventEmitter
A handle on the HandbrakeCLI process. Emits events you can monitor to track progress. An instance of this class is returned by [spawn](#module_handbrake-js.spawn).

**Kind**: inner class of [handbrake-js](#markdown-header-handbrakejs)  
**Extends:** EventEmitter  
**Emits**: [start](#markdown-header-start), [begin](#markdown-header-begin), [progress](#markdown-header-progressprogress), [output](#markdown-header-outputoutput), [error](#markdown-header-errorerror), [end](#markdown-header-end), [complete](#markdown-header-complete)  

* [~Handbrake](#markdown-header-hbjshandbrake-externaleventemitter) ⇐ EventEmitter
    * [.output](#markdown-header-handbrakeoutput-string) : string
    * [.options](#markdown-header-handbrakeoptions-object) : object
    * [.eError](#markdown-header-handbrakeeerror)
    * ["start"](#markdown-header-start)
    * ["begin"](#markdown-header-begin)
    * ["progress" (progress)](#markdown-header-progressprogress)
    * ["output" (output)](#markdown-header-outputoutput)
    * ["error" (error)](#markdown-header-errorerror)
    * ["end"](#markdown-header-end)
    * ["complete"](#markdown-header-complete)

#### handbrake.output : string
A `string` containing all handbrakeCLI output

**Kind**: instance property of Handbrake  
#### handbrake.options : object
a copy of the options passed to [spawn](#module_handbrake-js.spawn)

**Kind**: instance property of Handbrake  
#### handbrake.eError
All operational errors are emitted via the [error](#module_handbrake-js..Handbrake+event_error) event.

**Kind**: instance enum of Handbrake  
**Properties**

| Name | Default | Description |
| --- | --- | --- |
| VALIDATION | `ValidationError` | Thrown if you accidentally set identical input and output paths (which would clobber the input file), forget to specifiy an output path and other validation errors |
| INVALID_INPUT | `InvalidInput` | Thrown when the input file specified does not appear to be a video file |
| OTHER | `Other` | Thrown if Handbrake crashes |
| NOT_FOUND | `HandbrakeCLINotFound` | Thrown if the installed HandbrakeCLI binary has gone missing.. |

#### "start"
Fired as HandbrakeCLI is launched. Nothing has happened yet.

**Kind**: event emitted by Handbrake  
#### "begin"
Fired when encoding begins. If you're expecting an encode and this never fired, something went wrong.

**Kind**: event emitted by Handbrake  
#### "progress" (progress)
Fired at regular intervals passing a `progress` object.

**Kind**: event emitted by Handbrake  

| Param | Type | Description |
| --- | --- | --- |
| progress | object | details of encode progress |
| progress.taskNumber | number | current task index |
| progress.taskCount | number | total tasks in the queue |
| progress.percentComplete | number | percent complete |
| progress.fps | number | Frames per second |
| progress.avgFps | number | Average frames per second |
| progress.eta | string | Estimated time until completion |
| progress.task | string | Task description, either "Encoding" or "Muxing" |

#### "output" (output)
**Kind**: event emitted by Handbrake  

| Param | Type | Description |
| --- | --- | --- |
| output | string | An aggregate of `stdout` and `stderr` output from the underlying HandbrakeCLI process. |

#### "error" (error)
**Kind**: event emitted by Handbrake  

| Param | Type | Description |
| --- | --- | --- |
| error | Error | All operational exceptions are delivered via this event. |
| error.name | eError | The unique error identifier |
| error.message | string | Error description |
| error.errno | string | The HandbrakeCLI return code |

#### "end"
Fired on successful completion of an encoding task. Always follows a [begin](#module_handbrake-js..Handbrake+event_begin) event, with some {@link module:handbrake-js~Handbrake#event:progress} in between.

**Kind**: event emitted by Handbrake  
#### "complete"
Fired when HandbrakeCLI exited cleanly. This does not necessarily mean your encode completed as planned..

**Kind**: event emitted by Handbrake  
